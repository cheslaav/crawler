defmodule Crawler.Tenders.Tender do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tenders" do
    field :city, :string
    field :contact_person, :string
    field :email, :string
    field :fax, :string
    field :org_name, :string
    field :phone, :string
    field :site, :string
    field :tender_id, :string
    field :tender_name, :string
    field :tender_link, :string
    field :keyword, :string
    field :status, :string, default: "новый"

    timestamps()
  end

  @doc false
  def changeset(tender, attrs) do
    tender
    |> cast(attrs, [:tender_id, :tender_name, :org_name, :city, :site, :contact_person, :email, :phone, :fax, :tender_link, :keyword])
    |> validate_required([:tender_id, :tender_name, :org_name, :city, :site, :contact_person, :email, :phone, :fax, :tender_link, :keyword])
    |> unique_constraint(:tender_id)
  end
end
