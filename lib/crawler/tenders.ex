defmodule Crawler.Tenders do
  @moduledoc """
  The Tenders context.
  """
  @topic inspect(__MODULE__)

  import Ecto.Query, warn: false
  alias Crawler.Repo

  alias Crawler.Tenders.Tender

  

  def subscribe do
    Phoenix.PubSub.subscribe(Crawler.PubSub, @topic)
  end

  defp broadcast_change({:ok, result}, event) do
    Phoenix.PubSub.broadcast(Crawler.Tenders.PubSub, @topic, {__MODULE__, event, result})
  end  
  @doc """
  Returns the list of tenders.

  ## Examples

      iex> list_tenders()
      [%Tender{}, ...]

  """
  def list_tenders do
    Repo.all(Tender)
  end

  @doc """
  Gets a single tender.

  Raises `Ecto.NoResultsError` if the Tender does not exist.

  ## Examples

      iex> get_tender!(123)
      %Tender{}

      iex> get_tender!(456)
      ** (Ecto.NoResultsError)

  """
  def get_tender!(id), do: Repo.get!(Tender, id)

  @doc """
  Creates a tender.

  ## Examples

      iex> create_tender(%{field: value})
      {:ok, %Tender{}}

      iex> create_tender(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_tender(attrs \\ %{}) do
    %Tender{}
    |> Tender.changeset(attrs)
    |> Repo.insert()
    |> broadcast_change([:tender, :created])
  end

  @doc """
  Updates a tender.

  ## Examples

      iex> update_tender(tender, %{field: new_value})
      {:ok, %Tender{}}

      iex> update_tender(tender, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_tender(%Tender{} = tender, attrs) do
    tender
    |> Tender.changeset(attrs)
    |> Repo.update()
    |> broadcast_change([:tender, :updated])
  end

  @doc """
  Deletes a tender.

  ## Examples

      iex> delete_tender(tender)
      {:ok, %Tender{}}

      iex> delete_tender(tender)
      {:error, %Ecto.Changeset{}}

  """
  def delete_tender(%Tender{} = tender) do
    tender
    |> Repo.delete()
    |> broadcast_change([:tender, :deleted])
    end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking tender changes.

  ## Examples

      iex> change_tender(tender)
      %Ecto.Changeset{data: %Tender{}}

  """
  def change_tender(%Tender{} = tender, attrs \\ %{}) do
    Tender.changeset(tender, attrs)
  end
end
