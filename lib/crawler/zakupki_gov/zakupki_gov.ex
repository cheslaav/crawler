defmodule Crawler.ZakupkiGov do

# keyWordsArr = ['каталог', 'справочник', 'нормализация', 'классификация', 'верификация', 'нормативно', 'НСИ', 'МТР']

# http://zakupki.gov.ru/epz/order/quicksearch/search.html
# https://synapsenet.ru/
# https://www.b2b-center.ru/market/
# https://www.rts-tender.ru/auctionsearch
# http://www.sberbank-ast.ru/purchaseList.aspx
# https://www.fabrikant.ru/trades/procedure/search/

@base_url "https://zakupki.gov.ru"
@link_url "/epz/order/notice/ea44/view/common-info.html?regNumber="
@lines_per_page 10
@max_page_number 2

  def make_report(keyword) do
    #ToDo Получить кол-во страниц по запросу для pages
    pages = 1..@max_page_number
    Enum.reduce(pages, [], fn page, items_acc -> get_items(page, keyword) ++ items_acc end)
    |> Enum.map(fn item -> get_fields(item, keyword) end)
    |> Enum.filter(fn map -> Map.has_key?(map, :city) end)
  end

  @doc "Получает список элементов со страницы поиска"
  def get_items(page_number, keyword) do
    result_html = get_html("https://zakupki.gov.ru/epz/order/extendedsearch/results.html?searchString=#{keyword}&pageNumber=#{page_number}&recordsPerPage=_#{@lines_per_page}&fz44=on&af=on")
    get_page_items(result_html, ".registry-entry__form")
  end

  @doc "Получает данные из полей карточки"
  def get_fields(item, keyword) do
    try do
      result_html1 = get_html("#{@base_url}#{@link_url}#{item}")
      company_title = get_company_title(result_html1, ".sectionMainInfo__body > div:nth-child(2) > span:nth-child(2) > a:nth-child(1)")
      tender_title = get_tender_title(result_html1, ".sectionMainInfo__body > div:nth-child(1) > span:nth-child(2)")
      city = get_city(result_html1, ".wrapper > div:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(3) > span:nth-child(2)")
      contact_person = get_contact_person(result_html1, ".wrapper > div:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(5) > span:nth-child(2)")
      email = get_email(result_html1, ".wrapper > div:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(6) > span:nth-child(2)")
      phone = get_phone(result_html1, ".wrapper > div:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(7) > span:nth-child(2)")
      fax = get_fax(result_html1, ".wrapper > div:nth-child(4) > div:nth-child(1) > div:nth-child(1) > section:nth-child(8) > span:nth-child(2)")
      row = %{
        tender_id: item,
        org_name: company_title,
        tender_name: tender_title,
        city: city,
        site: "#{@base_url}",
        contact_person: contact_person,
        email: email,
        phone: phone,
        fax: fax,
        tender_link: "#{@base_url}#{@link_url}#{item}",
        keyword: keyword
      }
      Crawler.Tenders.create_tender(row)
      row
    rescue
      _error -> %{}
    end
  end
  @doc "получает html код страницы"
  def get_html(url) do
    case HTTPoison.get(URI.encode(url)) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} -> {:ok, body}
      {:ok, _} -> {:error, "Status not 200"}
      {:error, _} -> {:error, "HTTPoison error"}
    end
  end

  @doc "Получает элементы по поисковому запросу"
  def get_page_items({:ok, html}, selector) do
      html
      |> Floki.find(selector)
      |> Enum.filter(fn elem -> filter_stop_words(elem) end)
      |> Enum.filter(fn elem -> filter_status(elem) end)
      |> Floki.attribute("a", "href")
      |> Enum.filter(fn elem -> String.contains?(elem, "/epz/order/notice/ea44/view/common-info.html?regNumber=") end)
      |> Enum.map(fn elem -> String.replace(elem, "/epz/order/notice/ea44/view/common-info.html?regNumber=", "") end)
  end

  def filter_stop_words(elem) do
    stop_words = ["аренда", "закупаем", "печат", "на закупку", "баннер", "реклам", "полиграф"]
    result = Floki.find(elem, ".registry-entry__body-value")
    |> Floki.text(deep: true)
    |> String.downcase()
    |> String.contains?(stop_words)
    not result
  end

  def filter_status(elem) do
    status_words = ["подача заявок"]
    Floki.find(elem, ".registry-entry__header-mid__title")
    |> Floki.text(deep: true)
    |> String.downcase()
    |> String.contains?(status_words)
  end

  @doc "Получает название организации"
  def get_company_title({:ok, html}, selector) do
    try do
      [{_, _, [company_title]}] = html
      |> Floki.find(selector)
    company_title
    rescue
      _ -> "error"
    end
  end

  @doc "Получает название процедуры"
  def get_tender_title({:ok, html}, selector) do
    try do
      [{_, _, [tender_title]}] = html
      |> Floki.find(selector)
    tender_title
    rescue
      _ -> "error"
    end
  end

  @doc "Получает город организации"
  def get_city({:ok, html}, selector) do
    try do
      [{_, _, [city]}] = html
      |> Floki.find(selector)
    city
    rescue
      _ -> "error"
    end
  end

  @doc "Получает ФИО ответственного лица"
  def get_contact_person({:ok, html}, selector) do
    try do
      [{"span", [{"class", "section__info"}], [contact_person, {"br", [], []}]}] = html
      |> Floki.find(selector)
    String.trim(contact_person)
    rescue
      _ -> "error"
    end
  end

  @doc "Получает email ответственного лица"
  def get_email({:ok, html}, selector) do
    try do
      [{"span", [{"class", "section__info"}], [email]}] = html
      |> Floki.find(selector)
    String.trim(email)
    rescue
      _ -> "error"
    end
  end

  @doc "Получает номер телефона ответственного лица"
  def get_phone({:ok, html}, selector) do
    try do
      [{"span", [{"class", "section__info"}], [phone]}] = html
      |> Floki.find(selector)
    String.trim(phone)
    rescue
      _ -> "error"
    end
  end

  @doc "Получает номер факса ответственного лица"
  def get_fax({:ok, html}, selector) do
    try do
      [{"span", [{"class", "section__info"}], [fax]}] = html
      |> Floki.find(selector)
    String.trim(fax)
    rescue
      _ -> "error"
    end
  end

end
