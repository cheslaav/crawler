defmodule CrawlerWeb.TenderCardLive do
  use CrawlerWeb, :live_view

  alias Crawler.Tenders

  def mount(_params, _session, socket) do
    
    Tenders.subscribe()

    {:ok, fetch(socket)}
  end

  def handle_event("add", %{"tender" => tender}, socket) do
    Tenders.create_tender(tender)

    {:noreply, fetch(socket)}
  end

  def handle_info({Tenders, [tender | _], _}, socket) do
    {:noreply, fetch(socket)}
  end

  defp fetch(socket) do
    assign(socket, tenders: Tenders.list_tenders())
  end
end  