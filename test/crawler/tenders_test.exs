defmodule Crawler.TendersTest do
  use Crawler.DataCase

  alias Crawler.Tenders

  describe "tenders" do
    alias Crawler.Tenders.Tender

    @valid_attrs %{city: "some city", contact_person: "some contact_person", email: "some email", fax: "some fax", org_name: "some org_name", phone: "some phone", site: "some site", tender_id: "some tender_id"}
    @update_attrs %{city: "some updated city", contact_person: "some updated contact_person", email: "some updated email", fax: "some updated fax", org_name: "some updated org_name", phone: "some updated phone", site: "some updated site", tender_id: "some updated tender_id"}
    @invalid_attrs %{city: nil, contact_person: nil, email: nil, fax: nil, org_name: nil, phone: nil, site: nil, tender_id: nil}

    def tender_fixture(attrs \\ %{}) do
      {:ok, tender} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Tenders.create_tender()

      tender
    end

    test "list_tenders/0 returns all tenders" do
      tender = tender_fixture()
      assert Tenders.list_tenders() == [tender]
    end

    test "get_tender!/1 returns the tender with given id" do
      tender = tender_fixture()
      assert Tenders.get_tender!(tender.id) == tender
    end

    test "create_tender/1 with valid data creates a tender" do
      assert {:ok, %Tender{} = tender} = Tenders.create_tender(@valid_attrs)
      assert tender.city == "some city"
      assert tender.contact_person == "some contact_person"
      assert tender.email == "some email"
      assert tender.fax == "some fax"
      assert tender.org_name == "some org_name"
      assert tender.phone == "some phone"
      assert tender.site == "some site"
      assert tender.tender_id == "some tender_id"
    end

    test "create_tender/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tenders.create_tender(@invalid_attrs)
    end

    test "update_tender/2 with valid data updates the tender" do
      tender = tender_fixture()
      assert {:ok, %Tender{} = tender} = Tenders.update_tender(tender, @update_attrs)
      assert tender.city == "some updated city"
      assert tender.contact_person == "some updated contact_person"
      assert tender.email == "some updated email"
      assert tender.fax == "some updated fax"
      assert tender.org_name == "some updated org_name"
      assert tender.phone == "some updated phone"
      assert tender.site == "some updated site"
      assert tender.tender_id == "some updated tender_id"
    end

    test "update_tender/2 with invalid data returns error changeset" do
      tender = tender_fixture()
      assert {:error, %Ecto.Changeset{}} = Tenders.update_tender(tender, @invalid_attrs)
      assert tender == Tenders.get_tender!(tender.id)
    end

    test "delete_tender/1 deletes the tender" do
      tender = tender_fixture()
      assert {:ok, %Tender{}} = Tenders.delete_tender(tender)
      assert_raise Ecto.NoResultsError, fn -> Tenders.get_tender!(tender.id) end
    end

    test "change_tender/1 returns a tender changeset" do
      tender = tender_fixture()
      assert %Ecto.Changeset{} = Tenders.change_tender(tender)
    end
  end
end
