# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :crawler,
  ecto_repos: [Crawler.Repo]

# Configures the endpoint
config :crawler, CrawlerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/Agbp6w0g7WH60XGR4MZ5k1Ec8FuGdkF2S5k8DNnlpG4mPrXTRtqvl6hnckX4qCs",
  render_errors: [view: CrawlerWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Crawler.PubSub,
  live_view: [signing_salt: "4u6MOL6g"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
