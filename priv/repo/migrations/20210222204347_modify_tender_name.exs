defmodule Crawler.Repo.Migrations.ModifyTenderName do
  use Ecto.Migration

  def change do
    create table(:tenders) do
      add :tender_id, :string
      add :org_name, :string
      add :tender_name, :string
      add :city, :string
      add :site, :string
      add :contact_person, :string
      add :email, :string
      add :phone, :string
      add :fax, :string
      add :tender_link, :text
      add :keyword, :string
      add :status, :string, default: "новый"

      timestamps()
    end
  end
end
