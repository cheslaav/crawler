defmodule Crawler.Repo.Migrations.ModifyTenderName1 do
  use Ecto.Migration

  def change do
    create unique_index(:tenders, [:tender_id])
    alter table(:tenders) do
      modify :tender_name, :text
      modify :tender_link, :text
    end
  end
end
